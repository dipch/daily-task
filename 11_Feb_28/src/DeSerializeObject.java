import java.io.*;

public class DeSerializeObject {
    public static void main(String[] args)  {

        String serializedFileName = "MyObject.ser";
        try{
            FileInputStream fileStream = new FileInputStream(serializedFileName);
            ObjectInputStream os = new ObjectInputStream(fileStream);
            //Object one = os.readObject();
            //TestObject t1 = (TestObject) one;
            //System.out.println(t1);

            TestObject t2 = (TestObject)os.readObject();
            System.out.println(t2);


            os.close();

        }catch (FileNotFoundException e) {
            //e.printStackTrace();
            System.out.println("File "+ serializedFileName + "not found. Please verify it's location");
        }catch (IOException ex) {
            ex.printStackTrace();
        }catch(ClassNotFoundException exx) {
            System.out.println("Class not found");
            System.out.println(exx.getMessage());
        }


    }
}
