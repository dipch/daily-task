import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class SerializeObject {
    public static void main(String[] args) throws Exception {
        String outputFileName = "MyObject.ser";
        FileOutputStream fileStream = new FileOutputStream(outputFileName);
        ObjectOutputStream os = new ObjectOutputStream(fileStream);

        TestObject t1 = new TestObject("Farid", 22);
        System.out.println(t1);

        os.writeObject(t1);
        os.close();

    }
}
