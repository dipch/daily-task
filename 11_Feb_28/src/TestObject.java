import java.io.Serializable;

public class TestObject implements Serializable {
    private String name;
    private int age;

    public TestObject(String name, int age) {
        this.name = name;
        this.age = age;
    }
    public String toString() {
        return "name: " + name + " age: " + age;
    }
}
