package TestSEDE;

import java.io.Serializable;

public class Dog implements Serializable {
    private String color;
    private int weight;

    public Dog(String color, int weight) {
        this.color = color;
        this.weight = weight;
    }

    public String toString() {
        return "Dog{color=" +color +" weight="+ weight+"}";
    }
}
