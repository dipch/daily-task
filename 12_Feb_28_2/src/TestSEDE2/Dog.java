package TestSEDE2;

import java.io.Serializable;

public class Dog extends Animal implements Serializable {
    private String color;
    private int weight;

    public Dog(int numberOfLegs, String color, int weight) {
        super(numberOfLegs);
        this.color = color;
        this.weight = weight;
    }

    public String toString() {
        return "Dog{"+super.toString()+"color=" +color +" weight="+ weight+"}";
    }
}