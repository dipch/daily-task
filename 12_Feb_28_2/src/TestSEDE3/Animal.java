package TestSEDE3;

import java.io.Serializable;

public class Animal implements Serializable {
    private int numberOfLegs;
    public Animal(int numberOfLegs) {
        this.numberOfLegs = numberOfLegs;
    }

    public String toString() {
        return "{Animal: "+ numberOfLegs + "}";
    }
}
