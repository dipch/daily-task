package TestSEDE3;


import java.io.Serializable;

public class Dog implements Serializable {
    private String color;
    private int weight;
    private Animal animal1;

    public Dog(Animal animal,  String color, int weight) {
        this.animal1 = animal;
        this.color = color;
        this.weight = weight;
    }

    public String toString() {
        return "Dog{"+animal1+"color=" +color +" weight="+ weight+"}";
    }
}