package TestSEDE3;




import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class Serialize {
    @SuppressWarnings("all")
    public static void main(String[] args) {
        String file = "dog.ser";
        Animal animal1 = new Animal(4);
        Dog dog1 = new Dog(animal1, "Black", 7);
        try{
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(dog1);
            oos.writeObject(animal1);
            System.out.println(dog1);
            oos.close();
        }catch(FileNotFoundException e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }catch(IOException e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }catch(Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
