package TestTransientSEDE;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeSerialize {
    @SuppressWarnings("all")
    public static void main(String[] args) {
        String file = "dog.ser";
        try{
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);

            Dog dog2= (Dog) ois.readObject();
            System.out.println(dog2);
        }catch(FileNotFoundException e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }catch(IOException e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }catch(ClassNotFoundException e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
