package TestTransientSEDE;

import java.io.Serializable;
@SuppressWarnings("all")
public class Dog implements Serializable {
    transient String color;
    private int weight;

    public Dog(String color, int weight) {
        this.color = color;
        this.weight = weight;
    }

    public String toString() {
        return "Dog{color=" +color +" weight="+ weight+"}";
    }
}