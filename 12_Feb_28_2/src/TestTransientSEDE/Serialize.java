package TestTransientSEDE;
import java.io.*;

public class Serialize {
    @SuppressWarnings("all")
    public static void main(String[] args) {
        String file = "dog.ser";
        Dog dog1 = new Dog("Black", 7);
        try{
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(dog1);
            System.out.println(dog1);
            oos.close();
        }catch(FileNotFoundException e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }catch(IOException e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }catch(Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}

