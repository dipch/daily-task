import java.io.*;
public class CopyFile {
    public static void main(String[] args) throws Exception {
        String file = "Test_image.jpg";


        FileInputStream fis = new FileInputStream(file);
        //DataInputStream dis = new DataInputStream(fis);

        FileOutputStream fos = new FileOutputStream("copy_Test_image.txt");
        //DataOutputStream dos = new DataOutputStream(fos);

        int availableBytes = 0;

        while((availableBytes = fis.available())!=0){
            //System.out.println(dis.available());
            System.out.println(availableBytes);
            byte[] byteToRead = new byte[availableBytes];
            fis.read(byteToRead);
            for(int i=0; i<byteToRead.length; i++){
                fos.write(byteToRead[i]);
                //System.out.println(byteToRead[i]);
            }
        }

        fos.flush();
        fos.close();
        fis.close();
    }
 }

