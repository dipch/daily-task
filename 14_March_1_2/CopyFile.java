import java.io.*;
public class CopyFile {
    public static void main(String[] args) throws Exception {
        String file1 = args[0];
        String file2 = args[1];
        
        int numberOfArguments = args.length;
        System.out.println("Number of Arguments= "+numberOfArguments);

        if(numberOfArguments>0){
            for(int i=0; i<numberOfArguments; i++){
                System.out.println("Argument "+ i +" = "+ args[i]);
            }
        }

        FileInputStream fis = new FileInputStream(file1);
        DataInputStream dis = new DataInputStream(fis);

        FileOutputStream fos = new FileOutputStream(file2);
        DataOutputStream dos = new DataOutputStream(fos);

        int availableBytes = 0;
        while((availableBytes = dis.available())!=0){
            System.out.println("Available Bytes = "+availableBytes);
            byte[] byteToRead = new byte[availableBytes];
            dis.read(byteToRead);
            //Integer.toBinaryString();
            for(int i=0; i<byteToRead.length; i++){
                dos.write(byteToRead[i]);
                //System.out.println(byteToRead[i]);
            }

        }

        dos.flush();
        dos.close();
        dis.close();



    }
 }

