import java.io.*;
public class MainMethodArgument {
    public static void main(String[] args) throws Exception {
        int numberOfArguments = args.length;
        System.out.println("Number of Arguments= "+numberOfArguments);

        if(numberOfArguments>0){
            for(int i=0; i<numberOfArguments; i++){
                System.out.println("Argument "+ i +" = "+ args[i]);
            }
        }
    }
 }


