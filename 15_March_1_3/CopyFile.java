import java.io.*;
public class CopyFile {

    public static void main(String[] args) throws Exception {

        FileOutputStream fos = null;
        DataOutputStream dos = null;

        if (args.length != 2) {
            throw new IllegalArgumentException("Exactly 2 parameters required");
        }

        System.out.println("Number of Arguments= "+args.length);

        for(int i=0; i<args.length; i++){
            System.out.println("Argument "+ i +" = "+ args[i]);
        }

        File file1 = new File(args[0]);
        File file2 = new File(args[1]);

        if (file1.isDirectory() || file2.isDirectory())
            throw new IllegalArgumentException("Argument is a directory, not a file");


        try{
            FileInputStream fis = new FileInputStream(file1);
            DataInputStream dis = new DataInputStream(fis);

            fos = new FileOutputStream(file2);
            dos = new DataOutputStream(fos);


            int availableBytes = 0;
            while ((availableBytes = dis.available()) != 0) {
                System.out.println("Available Bytes = " + availableBytes);
                byte[] byteToRead = new byte[availableBytes];
                dis.read(byteToRead);
                for (int i = 0; i < byteToRead.length; i++) {
                    dos.write(byteToRead[i]);
                    //System.out.println(byteToRead[i]);
                }
            }
            System.out.println("File copied successfully");


        }catch (FileNotFoundException e){
            System.out.println(e.getMessage());
            //e.printStackTrace();
        }catch(IOException e) {
            System.out.println(e.getMessage());
        }catch (Exception e) {
            System.out.println(e.getMessage());
        }finally {
            br.close();
            fr.close();
            bw.close();
            fw.close();
        }

    }
}

