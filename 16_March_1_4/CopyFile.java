import java.io.*;
public class CopyFile {

    public static void main(String[] args) throws Exception {

        FileReader fr = null;
        BufferedReader br = null;
        FileWriter fw = null;
        BufferedWriter bw = null;

        if (args.length != 2) {
            throw new IllegalArgumentException("Exactly 2 parameters required");
        }

        System.out.println("Number of Arguments= "+args.length);

        for(int i=0; i<args.length; i++){
            System.out.println("Argument "+ i +" = "+ args[i]);
        }

        File file1 = new File(args[0]);
        File file2 = new File(args[1]);

        if (file1.isDirectory() || file2.isDirectory())
            throw new IllegalArgumentException("Argument is a directory, not a file");


        try{
            fr = new FileReader(file1);
            fw = new FileWriter(file2);
            br = new BufferedReader(fr);
            bw = new BufferedWriter(fw);

            String line;

            while ((line = br.readLine()) != null) {
                bw.write(line);
                bw.newLine();
            }

            System.out.println("File copied successfully");


        }catch (FileNotFoundException e){
            System.out.println(e.getMessage());
            //e.printStackTrace();
        }catch(IOException e) {
            System.out.println(e.getMessage());
        }catch (Exception e) {
            System.out.println(e.getMessage());
        }finally {
            br.close();
            fr.close();
            bw.close();
            fw.close();
        }

    }
}

