import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

public class CopyFile {

    public static void main(String[] args) throws Exception {
        int availableBytes = 0;
        int nab = 0;
        FileOutputStream fos = null;
        DataOutputStream dos = null;



        FileReader fr = null;
        BufferedReader br = null;

        FileInputStream fis = null;
        DataInputStream dis = null;


        FileInputStream nfis = null;
        DataInputStream ndis = null;
        FileOutputStream os = null;



        if (args.length != 2) {
            throw new IllegalArgumentException("Exactly 2 parameters required");
        }

        System.out.println("Number of Arguments= "+args.length);

        for(int i=0; i<args.length; i++){
            System.out.println("Argument "+ i +" = "+ args[i]);
        }

        File file1 = new File(args[0]);
        File file2 = new File(args[1]);
        

        if (file1.isDirectory() || file2.isDirectory())
            throw new IllegalArgumentException("Argument is a directory, not a file");


        try {
            nfis = new FileInputStream(file1);
            ndis = new DataInputStream(nfis);

            availableBytes = 0;
            while ((availableBytes = ndis.available()) != 0) {
                System.out.println("Available Bytes for new file = " + availableBytes);
                nab = availableBytes;
                break;
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }


        try {
            fr = new FileReader(file1);
            br = new BufferedReader(fr);
            fos = new FileOutputStream(file2);
            dos = new DataOutputStream(fos);

            String line;
            System.out.println("availableBytes: " + nab);

            while ((line = br.readLine()) != null) {
                Integer l = Integer.parseInt(line);
                //b[i] = ll.byteValue();
                dos.write(l);
            }

            System.out.println("File copied successfully");


        }catch (FileNotFoundException e){
            System.out.println(e.getMessage());
        }catch(IOException e) {
            System.out.println(e.getMessage());
        }catch (Exception e) {
            System.out.println(e.getMessage());
        }finally {

            try {
                if (fr != null && br != null && fos != null && dos != null) {

                    fr.close();
                    br.close();
                    fos.close();
                    dos.close();
                }
            }
            catch (Exception e) {
                System.out.println("Error while closing streams" + e);
            }
        }



    }
}
