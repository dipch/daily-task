import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ObjectDecode {
    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws FileNotFoundException, ArrayIndexOutOfBoundsException, IOException {

        String fileName = "Person.txt";
        FileReader fr = new FileReader(fileName);
        BufferedReader br = new BufferedReader(fr);

        String line;
        ArrayList<Person> persons = new ArrayList();

        while ((line = br.readLine()) != null) {
            String[] result = line.split(",");
            persons.add(new Person(Long.parseLong(result[0]), result[1], Integer.parseInt(result[2])));
        }

        System.out.println("File Name: " + fileName);
        System.out.println("Total Number of Objects: " + persons.size());

        for (Person p : persons) {
            System.out.println("Object: id="+ p.getId() + ", name=" + p.getName() + ", age=" + p.getAge());
        }

    }
}
