import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;


public class ObjectEncode{
    public static void main(String[] args) throws ArrayIndexOutOfBoundsException, FileNotFoundException, IOException{

        String fileName = "Person.txt";
        Person[] p = new Person[3];

        p[0] = new Person(85871L, "John", 23);
        p[1] = new Person(2L, "Jane", 21);
        p[2] = new Person(372036854775807L, "Jack", 25);
        System.out.println("Created 3 Person objects");

        FileWriter fw = new FileWriter(fileName);

        boolean flag = false;

        for(int i=0; i<3; i++){

            if(flag){
                fw.write(System.getProperty( "line.separator" ));
            }

            flag = true;
            fw.write(p[i].toString());
        }
        System.out.println("Encoded objects properties to file: "+fileName);

        fw.close();

    }
}
