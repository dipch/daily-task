public class Player {

    private int number = 0;

    /**
     * @return the guess number
     */
    public int getNumber() {
        return number;
    }

    /**
     * @param number the guess number to set
     */
    public void setNumber(int number) {
        this.number = number;
    }

    /**
     * @param playerName: the player name
     */
    public void guess(String playerName) {
        number = (int)(Math.random() * 10);
        System.out.println(playerName+ " guessed " + number);
    }
}


