import java.io.*;
import java.util.*;
import java.util.ArrayList;
import java.util.List;
public class UnionUtil<E, F> {
    public static <E extends Cake>List<E> union(List<E> list1, List<F> list2) {
        List<E> result = new ArrayList<E>();
        for (int i = 0; i < list1.size(); i++) {
            result.add(list1.get(i));
        }
        for (int i = 0; i < list2.size(); i++) {
            result.add((E) list2.get(i));
        }
        return result;
    }
}