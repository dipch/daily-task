import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class BiDirectionalSocketClient {
    public void go(){
        try{
            Socket s = new Socket("127.0.0.1", 4242);

            OutputStream stream = s.getOutputStream();
            //System.out.println("inet: "+s.getInetAddress());
            //System.out.println("local addr: "+s.getLocalAddress());
            //System.out.println("local port: "+s.getLocalPort());

            //File destination = new File("simple_text_writer.txt");
            //FileOutputStream stream = new FileOutputStream(destination);

            InputStreamReader streamReader = new InputStreamReader(s.getInputStream());
            BufferedReader reader = new BufferedReader(streamReader);
            PrintWriter writer = new PrintWriter(stream);


            while (true){

                System.out.println("Enter text: ");
                Scanner scanner = new Scanner(System.in);
                String input = scanner.nextLine();

                writer.println(input);
                writer.flush();
                //writer.close();
                System.out.println("Message sent.");

                String response = reader.readLine();
                System.out.println("Response received: " + response);
                //writer.close();
            }

        }catch(IOException e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        BiDirectionalSocketClient client = new BiDirectionalSocketClient();
        client.go();
    }
}
