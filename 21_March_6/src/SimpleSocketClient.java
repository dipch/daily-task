import java.io.*;
import java.net.Socket;

public class SimpleSocketClient {
    public void go(){
        try{
            Socket s = new Socket("127.0.0.1", 4242);

            OutputStream stream = s.getOutputStream();
            System.out.println("inet: "+s.getInetAddress());
            System.out.println("local addr: "+s.getLocalAddress());
            System.out.println("local port: "+s.getLocalPort());

            //File destination = new File("simple_text_writer.txt");
            //FileOutputStream stream = new FileOutputStream(destination);
            PrintWriter writer = new PrintWriter(stream);
            writer.println("Hello, world!");
            System.out.println("Message sent.");
            writer.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        SimpleSocketClient client = new SimpleSocketClient();
        client.go();
    }
}
