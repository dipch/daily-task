import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

public class SimpleTextWriter {
    public void go(){
        try{
            File destination = new File("simple_text_writer.txt");
            FileOutputStream stream = new FileOutputStream(destination);
            PrintWriter writer = new PrintWriter(stream);
            writer.println("Hello, world!");
            writer.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        SimpleTextWriter client = new SimpleTextWriter();
        client.go();
    }
}
