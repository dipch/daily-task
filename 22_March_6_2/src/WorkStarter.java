public class WorkStarter {
    public static void main(String[] args) {
        WorkerThread wt1 = new WorkerThread(1);
        WorkerThread wt2 = new WorkerThread(2);

        wt1.start();
        wt2.start();
    }
}
