public class WorkerThread extends Thread {
    private int serialNo;

    public WorkerThread(int serialNo) {
        this.serialNo = serialNo;
    }

    public void run() {
        for(int i = 0; i < 100; i++) {
            System.out.println("WorkerThread no: " + serialNo + " counting " + i + " out of 100");
            try{
                Thread.sleep(100);
            }catch(Exception e){
                System.out.println(e);
            }

        }
    }
}
