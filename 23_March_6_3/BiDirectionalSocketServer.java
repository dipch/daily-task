
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class BiDirectionalSocketServer {
    public void go() {
        try {
            ServerSocket serverSocket = new ServerSocket(4242);
            while (true) {
                Socket socket = serverSocket.accept();
                InputStreamReader streamReader = new InputStreamReader(socket.getInputStream());
                BufferedReader reader = new BufferedReader(streamReader);

                OutputStream streamWriter = socket.getOutputStream();
                PrintWriter writer = new PrintWriter(streamWriter);



                String request = reader.readLine();
                writer.println(request.toUpperCase());
                writer.flush();

                System.out.println("Request received: " + request);
                System.out.println("Response sent: " + request.toUpperCase());
                reader.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        BiDirectionalSocketServer server = new BiDirectionalSocketServer();
        server.go();
    }
}

