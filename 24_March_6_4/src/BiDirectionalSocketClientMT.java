import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class BiDirectionalSocketClientMT {
    public void go(){
        try{
            Socket s = new Socket("127.0.0.1", 4242);
            OutputStream stream = s.getOutputStream();
            InputStreamReader streamReader = new InputStreamReader(s.getInputStream());
            BufferedReader reader = new BufferedReader(streamReader);
            PrintWriter writer = new PrintWriter(stream);


            while (true){



                System.out.println("Enter text: ");
                Scanner scanner = new Scanner(System.in);
                String input = scanner.nextLine();

                writer.println(input);
                writer.flush();
                //writer.close();
                System.out.println("Message sent.");

                String response = reader.readLine();
                System.out.println("Response received: " + response);
                //writer.close();
            }

        }catch(IOException e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        BiDirectionalSocketClientMT client = new BiDirectionalSocketClientMT();
        client.go();
    }
}
