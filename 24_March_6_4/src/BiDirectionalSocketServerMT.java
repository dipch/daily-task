import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class BiDirectionalSocketServerMT {
    public void go(){
        try{
            ServerSocket serverSocket = new ServerSocket(4242);
            Socket socket = null;

            while(true){
                socket = serverSocket.accept();
                new RequestHandlerThread(socket).start();

            }
        }catch(IOException ex){
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        BiDirectionalSocketServerMT server = new BiDirectionalSocketServerMT();
        server.go();
    }
}
