import java.io.*;
import java.net.Socket;

public class RequestHandlerThread extends Thread{
    private Socket socket;

    public RequestHandlerThread(Socket socket) {
        this.socket = socket;
    }

    public void run() {
        try {
            InputStreamReader streamReader = new InputStreamReader(socket.getInputStream());
            BufferedReader reader = new BufferedReader(streamReader);

            OutputStream stream = socket.getOutputStream();
            PrintWriter writer = new PrintWriter(stream, true);

            String request = null;

            while ((request = reader.readLine()) != null) {
                writer.println(request.toUpperCase());
                System.out.println(": "+socket.getInetAddress());
                System.out.println(": "+socket.getChannel());
                System.out.println(": "+socket.getLocalSocketAddress());
                System.out.println(": "+socket.getLocalAddress());
                System.out.println(": "+socket.getLocalPort());

                System.out.println(": "+socket.getPort());
                System.out.println(": "+socket.getRemoteSocketAddress());
                System.out.println(": "+socket.getReceiveBufferSize());

                System.out.println("Request received: " + request);
                System.out.println("Response sent: " + request.toUpperCase());

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
