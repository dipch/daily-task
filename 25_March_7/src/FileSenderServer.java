import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Handler;

public class FileSenderServer {

    public void go() {
        try {
            ServerSocket serverSocket = new ServerSocket(4242);
            Socket socket = null;
            while (true) {
                socket = serverSocket.accept();
                new RequestHandlerThread(socket).start();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    public static void main(String[] args) {
        FileSenderServer server = new FileSenderServer();
        server.go();

    }
}
