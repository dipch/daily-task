import java.io.*;
import java.net.Socket;

public class RequestHandlerThread extends Thread{
    private Socket socket;
    public RequestHandlerThread(Socket socket) {
        this.socket = socket;
    }

    public void run() {
        try {
            InputStreamReader streamReader = new InputStreamReader(socket.getInputStream());
            BufferedReader reader = new BufferedReader(streamReader);

            String request = reader.readLine();

            System.out.println("===============================");
            System.out.println("=====:Client Information:======");
            System.out.println("Inet Address: "+socket.getInetAddress());
            System.out.println("Channel: "+socket.getChannel());
            System.out.println("Local Socket Addr: "+socket.getLocalSocketAddress());
            System.out.println("Local Addr: "+socket.getLocalAddress());
            System.out.println("Local Port: "+socket.getLocalPort());
            System.out.println("Port: "+socket.getPort());
            System.out.println("Remote Socket Addr: "+socket.getRemoteSocketAddress());
            System.out.println("Receive Buffer Size: "+socket.getReceiveBufferSize());
            System.out.println("===============================");
            System.out.println("File Name: "+request);

            File file = new File(request);

            FileInputStream fis = new FileInputStream(file);
            BufferedInputStream bis = new BufferedInputStream(fis);

            OutputStream streamWriter = socket.getOutputStream();
            BufferedOutputStream bos = new BufferedOutputStream(streamWriter);

//                reader.close();

            if (file.exists()) {

                System.out.println("File found on the server ");
                int FileSize = (int) file.length();
                System.out.println("File Size: "+FileSize);



                int availableBits = 0;
                byte[] byteToRead = null;
                while ((availableBits = bis.available()) != 0) {
                    byteToRead = new byte[2000];
                    bis.read(byteToRead);
                    bos.write(byteToRead);
                }

                bos.flush();
                bos.close();
                streamWriter.flush();
                streamWriter.close();


                System.out.println("File sent to the client");
                System.out.println("===============================");
            } else {
                System.out.println("File not found on server ");
                System.out.println("===============================");
            }
            reader.close();

        }catch(IOException e) {
            e.printStackTrace();
        }
    }

}
