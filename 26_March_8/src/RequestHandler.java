import java.net.Socket;
import java.io.*;
import java.nio.charset.StandardCharsets;

public class RequestHandler extends Thread{
    private Socket socket;
    public RequestHandler(Socket socket) {
        this.socket = socket;
    }

    public void run(){
        try{
           // InputStreamReader streamReader = new InputStreamReader(socket.getInputStream());
            //BufferedReader reader = new BufferedReader(streamReader);
            //String fileName = reader.readLine();

            OutputStreamWriter osw = new OutputStreamWriter(socket.getOutputStream());
            BufferedWriter bw = new BufferedWriter(osw);

            String fileName = "a.html";
            File file = new File(fileName);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);

            if(!file.exists() ) {
                System.out.println("Input File name: "+fileName);
                bw.write("File not found");
                bw.newLine();
                bw.flush();
            }
            else if(file.isDirectory()){
                System.out.println("Input File name: "+fileName);
                bw.write("File is a directory");
                bw.newLine();
                bw.flush();
            }
            else if(!file.canRead()){
                System.out.println("Input File name: "+fileName);
                bw.write("Cannot read file");
                bw.newLine();
                bw.flush();
            }
            else{
                String line = null;
                System.out.println("Input File name: "+fileName);
                System.out.println("Sending file content...");

                //bw.write("HTTP/1.1 200 OK\n");
                //bw.write("\r\n");

                while ((line = br.readLine()) != null) {

                    bw.write(line);
                    bw.newLine();
                    bw.flush();
                    //bw.newLine();
                }
                System.out.println("File sent.");
            }


            bw.flush();
            //reader.close();
            br.close();
            fr.close();
            bw.close();
            osw.close();


            socket.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }


}
