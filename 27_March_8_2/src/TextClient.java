import java.io.*;
import java.net.Socket;

public class TextClient {
    public void go(){

        try{
            Socket socket = new Socket("127.0.0.1", 4242);
/*
            OutputStream stream = socket.getOutputStream();
            PrintWriter writer = new PrintWriter(stream);

            System.out.println("Enter a file name: ");
            Scanner scan = new Scanner(System.in);
            String fileName = scan.nextLine();
            writer.println(fileName);
            writer.flush();
*/
            InputStreamReader inputStreamReader = new InputStreamReader(socket.getInputStream());
            BufferedReader reader = new BufferedReader(inputStreamReader);

            String line = null;
            System.out.println("Server response...");
            System.out.println("=============================");

            while((line = reader.readLine()) != null) {
                System.out.println(line);
            }

            System.out.println("=============================");
            System.out.println("Text Received");

            //writer.close();


        }catch(Exception e){
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        TextClient client = new TextClient();
        client.go();
    }
}
