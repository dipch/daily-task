import java.io.*;
import java.net.*;

public class TextServer{
    public void go(){
        try{

            ServerSocket serverSocket = new ServerSocket(4242);
            Socket socket = serverSocket.accept();

            OutputStreamWriter out = new OutputStreamWriter(socket.getOutputStream());
            PrintWriter writer = new PrintWriter(out);

            FileReader fileReader = new FileReader("a.txt");
            System.out.println("Server Response: " );

            int character = 0;

            while((character = fileReader.read()) != -1){
                writer.write(character);
                //System.out.println((char)character);
            }

            writer.close();
            fileReader.close();

        } catch (IOException ex){
            ex.printStackTrace();
        }
    }

    public static void main(String[] args){

        TextServer server = new TextServer();
        server.go();
    }
}