import java.net.Socket;
import java.io.*;
import java.nio.charset.StandardCharsets;

public class RequestHandler extends Thread{
    private Socket socket;
    public RequestHandler(Socket socket) {
        this.socket = socket;
    }

    public void run(){
        try{
            //InputStreamReader streamReader = new InputStreamReader(socket.getInputStream());
            //BufferedReader reader = new BufferedReader(streamReader);
            //String fileName = reader.readLine();

            //OutputStreamWriter osw = new OutputStreamWriter(socket.getOutputStream());
            //BufferedWriter bw = new BufferedWriter(osw);
            OutputStream os = socket.getOutputStream();



            String fileName = "a.txt";
            File file = new File(fileName);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);


            String line = null;
            System.out.println("Input File name: "+fileName);
            System.out.println("Sending file content...");

            os.write("HTTP/1.1 200 OK\n".getBytes());
            os.write("\r\n".getBytes());
            os.write("<b>Ouput Text:</b>\n".getBytes());
            os.write("<br>".getBytes());
            String newline = "\n";



            while ((line = br.readLine()) != null) {

                //bw.write(line);
                os.write(line.getBytes());
                os.write("<br>".getBytes());
            }
            os.write("<br>".getBytes());
            os.write("<b>hello</b>\r\n".getBytes());
            os.flush();
            os.close();
            System.out.println("File sent.");


            socket.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }


}
