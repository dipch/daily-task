import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class TextServer {

    public void go() {
        try {
            ServerSocket serverSocket = new ServerSocket(4242);
            Socket socket = null;
            while (true) {
                socket = serverSocket.accept();
                new RequestHandler(socket).start();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    /*
    public void go(){
        try{
            ServerSocket serverSocket = new ServerSocket(4242);
            while(true){
                Socket socket = serverSocket.accept();

                InputStreamReader streamReader = new InputStreamReader(socket.getInputStream());
                BufferedReader reader = new BufferedReader(streamReader);
                String fileName = reader.readLine();

                OutputStreamWriter osw = new OutputStreamWriter(socket.getOutputStream());
                BufferedWriter bw = new BufferedWriter(osw);

                //String fileName = "a.txt";
                File file = new File(fileName);
                FileReader fr = new FileReader(file);
                BufferedReader br = new BufferedReader(fr);

                if(!file.exists() ) {
                    System.out.println("Input File name: "+fileName);
                    bw.write("File not found");
                    bw.newLine();
                    bw.flush();
                }
                else if(file.isDirectory()){
                    System.out.println("Input File name: "+fileName);
                    bw.write("File is a directory");
                    bw.newLine();
                    bw.flush();
                }
                else if(!file.canRead()){
                    System.out.println("Input File name: "+fileName);
                    bw.write("Cannot read file");
                    bw.newLine();
                    bw.flush();
                }
                else{
                    String line = null;
                    System.out.println("Input File name: "+fileName);
                    System.out.println("Sending file content...");
                    while ((line = br.readLine()) != null) {

                        bw.write(line);
                        bw.newLine();
                        bw.flush();
                        //bw.newLine();
                    }
                    System.out.println("File sent.");
                }


                bw.flush();
                reader.close();
                br.close();
                fr.close();
                bw.close();
                osw.close();
            }

        }catch(IOException ioe){
            ioe.printStackTrace();
        }
    }
     */

    public static void main(String[] args){
        TextServer ts = new TextServer();
        ts.go();
    }
}
