import java.io.*;
import java.net.Socket;
import java.util.Base64;


public class FileClient {
    public void go(){

        String serverHost = "127.0.0.1";
        int serverPort = 4242;

        try(Socket socket = new Socket(serverHost, serverPort)){

            //String fileName = "b.txt";
            String fileName = "to.jpg";
            System.out.println("Output File name: " + fileName);

            BufferedReader br = new BufferedReader(
                                    new InputStreamReader(
                                        socket.getInputStream()));
            FileOutputStream fos = new FileOutputStream(fileName);

            try(br;fos){

                String line = br.readLine();
                byte[] decodedBytes = Base64.getDecoder().decode(line);

                fos.write(decodedBytes);
                fos.flush();

                System.out.println("File Received");

            }catch(Exception e){
                e.printStackTrace();
            }

        }catch(Exception e){
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        FileClient client = new FileClient();
        client.go();
    }
}
