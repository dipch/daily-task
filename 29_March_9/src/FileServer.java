import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

class RequestHandler extends Thread{
    private final Socket socket;
    public RequestHandler(Socket socket) {
        this.socket = socket;
    }

    public void run(){
        try(BufferedWriter bw = new BufferedWriter(
                                    new OutputStreamWriter(socket.getOutputStream()))){

            //String fileName = "a.txt";
            String fileName = "ti.jpg";
            File file = new File(fileName);
            System.out.println("Input File name: "+fileName);
            System.out.println("Sending file content...");
            FileInputStream fis = new FileInputStream(file);

            try(fis){

                byte[] byteArray = fis.readAllBytes();
                byte[] encodedBytes = Base64.getEncoder().encode(byteArray);
                bw.write(new String(encodedBytes, StandardCharsets.UTF_8));
                bw.flush();
                System.out.println("File sent.");

            }catch(IOException e){
                System.out.println("Error: "+e.getMessage());
            }

        }catch(Exception e){
            e.printStackTrace();
        }
    }
}

public class FileServer {

    public void go() {

        int serverPort = 4242;
        try (ServerSocket serverSocket = new ServerSocket(serverPort)){

            System.out.println("Server is running...");

            while (true) {
                Socket socket = serverSocket.accept();
                new RequestHandler(socket).start();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args){
        FileServer fs = new FileServer();
        fs.go();
    }
}
