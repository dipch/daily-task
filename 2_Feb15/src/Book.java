import java.time.LocalDate;
import java.util.Date;
import java.text.SimpleDateFormat;

public class Book {
    private int book_id;
    private String book_author;
    private String book_name;
    private String ISBN;
    private LocalDate Date;
    private Date Date1;

    //private SimpleDateFormat date1;

    //published_date;

    /**
     *
     * @param book_name
     * @param book_id
     * @param book_author
     * @param ISBN
     * @param Date
     */
    public Book(String book_name, int book_id, String book_author, String ISBN, LocalDate Date) {
        this.book_id = book_id;
        this.book_author = book_author;
        this.book_name = book_name;
        this.ISBN = ISBN;
        this.Date = Date;
//        SimpleDateFormat date1 = new SimpleDateFormat("dd/MM/YYYY");
//        this.Date = date1.format(new Date());
//        this.Date = date1;

    }

    public Book(String book_name, int book_id, String book_author, String ISBN, Date Date1) {
        this.book_id = book_id;
        this.book_author = book_author;
        this.book_name = book_name;
        this.ISBN = ISBN;
        this.Date1 = Date1;

    }

    /**
     *
     * @return book_id
     */
    public int getBook_id() {
        return book_id;
    }

    /**
     *
     * @param book_id
     */
    public void setBook_id(int book_id) {
        this.book_id = book_id;
    }

    /**
     *
     * @return  book_author
     */
    public String getBook_author() {
        return book_author;
    }

    /**
     *
     * @param book_author
     */
    public void setBook_author(String book_author) {
        this.book_author = book_author;
    }

    /**
     *
     * @return book_name
     */
    public String getBook_name() {
        return book_name;
    }

    /**
     *
     * @param book_name
     */
    public void setBook_name(String book_name) {
        this.book_name = book_name;
    }

    /**
     *
     * @return ISBN
     */
    public String getISBN() {
        return ISBN;
    }

    /**
     *
     * @param ISBN
     */
    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    /**
     *  @return the Date
     *
     */
//    public LocalDate getDate() {
//        //return sDate;
//        return Date;
//    }

    /**
     * @param Date: the Date to set
     *
     */
    public void setDate(LocalDate Date) {
        //this.sDate = sDate;
        this.Date = Date;
    }

    public Date getDate() {
        //return sDate;
        return Date1;
    }

    

    public void setDate(Date Date1) {
        //this.sDate = sDate;
        this.Date1 = Date1;
    }
//
//    public void printDetails(){
//        System.out.println("-----------------------------------");
//        System.out.println("Book Name: " + this.book_name);
//        System.out.println("Book Author: " + this.book_author);
//        System.out.println("Book ID: " + this.book_id);
//        System.out.println("ISBN: " + this.ISBN);
//        System.out.println("Date: " + this.Date);
//    }
}


