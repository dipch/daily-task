import java.util.Date;
import java.time.*;
import java.text.SimpleDateFormat;
public class Main {
    public static void main(String[] args) throws Exception {

        Book[] b = new Book[5];

        LocalDate ld0 = LocalDate.of( 2026 , 1 , 11 );
        LocalDate ld1 = LocalDate.of( 2025 , 2 , 21 );
        LocalDate ld2 = LocalDate.of( 2024 , 3 , 30 );
        LocalDate ld3 = LocalDate.of( 2023 , 4 , 8 );
        LocalDate ld4 = LocalDate.of( 2022 , 5 , 15 );



        String sDate1="31/12/1997";
        String sDate2="31/12/1996";
        String sDate3="31/12/1995";
        String sDate4="31/12/1994";
        String sDate5="31/12/1993";


        Date d0=new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
        Date d1=new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
        Date d2=new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
        Date d3=new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
        Date d4=new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);


        b[0] = new Book("Java", 12345, "Alex", "978-3-16-148410-0", d0);
        b[1] = new Book("C++", 23456, "Smith", "978-1-4028-9462-6", d1);
        b[2] = new Book("C#", 34567, "John", "978-92-95055-02-5", d2);
        b[3] = new Book("Python", 45678, "Dave", "978-92-95055-02-9", d3);
        b[4] = new Book("Ruby", 56789, "Bob", "978-92-95099-02-1", d4);

        Print[] p = new Print[5];

        for(int i=0; i<5; i++){
            /**
             * Alternate (call & initialize at the same line)
             * final int j = i;
             *             p[i] = new Print() {{
             *                 printDetails(b[j]);
             *             }};
             */
            p[i] = new Print();
            p[i].printDetails(b[i]);
        }



//        Print p1 = new Print();
//        Print p2 = new Print();
//        Print p3 = new Print();
//        Print p4 = new Print();
//        Print p5 = new Print();





//        p1.printDetails(b1);
//        p2.printDetails(b2);
//        p3.printDetails(b3);
//        p4.printDetails(b4);
//        p5.printDetails(b5);

//        b1.printDetails();
//        b2.printDetails();
//        b3.printDetails();
//        b4.printDetails();
//        b5.printDetails();


//        System.out.println(b1.getBook_name());
//        System.out.println(b1.getBook_author());
//
//        System.out.println(b2.getBook_name());
//        System.out.println(b2.getBook_author());
//
//        System.out.println(b3.getBook_name());
//        System.out.println(b3.getBook_author());
//
//        System.out.println(b4.getBook_name());
//        System.out.println(b4.getBook_author());
//
//        System.out.println(b5.getBook_name());
//        System.out.println(b5.getBook_author());



    }
}
