public class Answer {
    private String ans;
    public Answer(String ans){
        this.ans = ans;
    }
    public String getAnswer(){
        return ans;
    }
    public void setAnswer(String ans){
        this.ans = ans;
    }
    public String toString(){
        return ans;
    }

}


