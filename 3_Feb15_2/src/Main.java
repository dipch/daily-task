import java.util.*;

public class Main {
    public static void main(String[] args) {
        Question[] q = new Question[5];
        q[0] = new Question(1, "What is Java?", 5, new Answer("Java is a programming Language"));
        q[1] = new Question(2, "Is Java a platform independent language?", 10, new Answer("Yes"));
        q[2] = new Question(3, "Who designed Java?",5,  new Answer("James Gosling"));
        q[3] = new Question(4, "When was Java Developed?", 5, new Answer("In the early 1990s"));
        q[4] = new Question(5, "What is the lates version of Java?",15,  new Answer("Java 17"));

        for(int i=0; i<5; i++){
            System.out.println(q[i].getQues() + "\t" + q[i].getAns());
        }







    }
}
