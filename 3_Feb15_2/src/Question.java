public class Question {
    private Answer ans;

    private String ques;
    private int questionId;
    private int marks;


    public Question(int questionId, String ques, int marks, Answer ans){
        this.ques = ques;
        this.questionId = questionId;
        this.marks = marks;
        this.ans = ans;
    }

    public Answer getAns() {
        return ans;
    }

    public void setAns(Answer ans) {
        this.ans = ans;
    }

    public String getQues() {
        return ques;
    }

    public void setQues(String ques) {
        this.ques = ques;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public int getMarks() {
        return marks;
    }

    public void setMarks(int marks) {
        this.marks = marks;
    }

    public void printS(){
        System.out.println(ques + " "+ ans);
        //return ques + " " + ans.toString() + "\n";
    }
}

