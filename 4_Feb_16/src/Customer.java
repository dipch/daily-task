public class Customer {
    private int customerId;
    private String customerName;
    private String customerAddress;
    private String customerPhone;

    public Customer(int customerId, String customerName, String customerAddress, String customerPhone) {
        this.customerId = customerId;
        this.customerName = customerName;
        this.customerAddress = customerAddress;
        this.customerPhone = customerPhone;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setcustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getcustomerName() {
        return customerName;
    }

    public void setcustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getcustomerAddress() {
        return customerAddress;
    }

    public void setcustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getcustomerPhone() {
        return customerPhone;
    }

    public String toString() {
        //return  customerId + customerName + customerAddress + customerPhone ;
        return customerId + "";
    }
}






