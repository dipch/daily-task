import java.util.ArrayList;
import java.util.List;

public class Invoice {
    private int invoiceId;

    private Customer customer;

    //private Product product;
    //List<Product> pro;
    ArrayList<Product> pro;// = new ArrayList<>();
    

    public Invoice(int invoiceId, Customer customer,  ArrayList<Product> pro) {
        this.invoiceId = invoiceId;
        this.customer = customer;
        //this.product = product;
        this.pro = pro;
    }
    public int getinvoiceId() {
        return invoiceId;
    }

    public void setOrderId(int invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Customer getCustomerId() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }



    public void setPro(ArrayList<Product> pro) {
        this.pro = pro;
    }

    public ArrayList<Product> getPro() {
        return pro;
    }



    public String getCustomerName(){
        return customer.getcustomerName();
    }
    public String getCustomerPhone(){
        return customer.getcustomerPhone();
    }
    public String getCustomerAddress(){
        return customer.getcustomerAddress();
    }








}



