import java.util.ArrayList;
import java.util.List;

public class Main {
    static ArrayList<Product> pd = new ArrayList<Product>();
    static ArrayList<Product> pd2 = new ArrayList<Product>();
    static ArrayList<Customer> cus = new ArrayList<Customer>();
    static ArrayList<Invoice> iv = new ArrayList<Invoice>();
    //static List<List<Invoice>> iv = new ArrayList<List<Invoice>>();


    public static void main(String[] args) {


        //cus.add(new Customer(1, "John", "Dhaka", "00123456789"));
        //cus.add(new Customer(1, "Alex", "Dhaka", "90123456789"));
        //cus.add(new Customer(1, "John", "Dhaka", "00123456789"));
        //cus.add(new Customer(1, "John", "Dhaka", "00123456789"));

        pd.add(new Product(1, "Thinkpad T480", 450.00));
        pd.add(new Product(2, "Thinkpad X1C", 1450.00));
        pd.add(new Product(3, "Thinkpad X390", 780.50));
        pd.add(new Product(4, "Thinkpad P52s", 999.99));

        pd2.add(new Product(1, "Thinkpad T480", 450.00));
        pd2.add(new Product(2, "Thinkpad X1C", 1450.00));


        Invoice iv1 = new Invoice(1,new Customer(1, "John", "Dhaka", "00123456789"), pd);
        Invoice iv2 = new Invoice(2,new Customer(2, "Alex", "Khulna", "99123456789"), pd2);

        iv.add(iv1);
        iv.add(iv2);


        //iv.add((new Invoice(1,new Customer(1, "John", "Dhaka", "00123456789"), pd) ));


        for(int i=0; i<iv.size(); i++){
            System.out.println( "|-----------------------------------|");
            System.out.println("Invoice ID: "+ iv.get(i).getinvoiceId());
            System.out.println( "|-----------------------------------|");
            System.out.println( "| Customer ID     : " + iv.get(i).getCustomerId());
            System.out.println( "| Customer Name   : " + iv.get(i).getCustomerName());
            System.out.println( "| Customer Address: " + iv.get(i).getCustomerAddress());
            System.out.println( "| Customer Phone  : " + iv.get(i).getCustomerPhone());
            System.out.println( "|-----------------------------------|");
            System.out.println( "|" + " Product ID \t" + "Product Name \t"+ "Product Price \t"  +"|");
            for(int j=0; j<iv.get(i).pro.size(); j++){
                System.out.print("| "+ iv.get(i).pro.get(j).getproductName()+ "\t");
                System.out.print("| "+ iv.get(i).pro.get(j).getproductId()+ "\t\t\t\t");
                System.out.print("| "+iv.get(i).pro.get(j).getproductPrice()+ "\t\n");
                System.out.println( "|-----------------------------------|");
            }
        }
    }
}