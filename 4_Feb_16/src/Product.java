public class Product {
    private int productId;
    private String productName;
    private double productPrice;

    private double bill;
    //private int productStock;

    public Product(int productId, String productName, double productPrice) {
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        //this.productStock = productStock;
    }

    public void setproductId(int productId) {
        this.productId = productId;
    }

    public int getproductId() {
        return productId;
    }

    public void setproductName(String productName) {
        this.productName = productName;
    }

    public String getproductName() {
        return productName;
    }

    public void setproductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public double getproductPrice() {
        return productPrice;
    }

//    public void setproductStock(int productStock) {
//        this.productStock = productStock;
//    }
//
//    public int getproductStock() {
//        return productStock;
//    }

    public String toString(){
        return "Product ID: " + productId + "\nProduct Name: " + productName + "\nProduct Price: " + productPrice ;
    }
}

