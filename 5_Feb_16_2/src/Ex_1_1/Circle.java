package Ex_1_1;

public class Circle {
    private double radius;
    private String color;

    //Constructs a Circle instance with default value for radius and color
    public Circle(){
        radius = 1.0;
        color = "red";
    }

    // Constructs a Circle instance with the given radius and default color
    public Circle(double r){
        radius = r;
        color = "red";
    }

    public double getRadius(){
        return radius;
    }

    public double getArea(){
        return Math.PI * radius * radius;
    }
}
