package Ex_1_1;

public class TestCircle {
    public static void main(String[] args) {
        Circle c1 = new Circle();
        Circle c2 = new Circle(5);
        System.out.println("The Circle c1 has radius of " + c1.getRadius() + " and area of " +c1.getArea());
        System.out.println("The Circle c2 has radius of " + c2.getRadius() + " and area of " +c2.getArea());

    }
}
