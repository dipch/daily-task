package Ex_2_10;
import Ex_2_8.MyPoint;
import Ex_2_9.MyTriangle;

public class MyRectangle {
    //Ex_2_8.MyPoint v1;
    Ex_2_8.MyPoint v2;
    Ex_2_8.MyPoint v3;
    //Ex_2_8.MyPoint v4;

    /**
    v3========v4
     |         |
     |         |
     v1=======v2

     */

    public MyRectangle(MyPoint v2, MyPoint v3){

        this.v2 = v2;
        this.v3 = v3;

    }
    public MyRectangle(int x1, int y1, int x2, int y2){
        v2 = new MyPoint(x1, y1);
        v3 = new MyPoint(x2, y2);
    }

    public double getArea(){
        //return v1.distance(v2) * v1.distance(v3);
        return Math.abs(( v3.getX()-v2.getX() ) * (v3.getY() - v2.getY()));
    }

    public double getPerimeter(){
        return Math.abs( 2* ( (v3.getX()-v2.getX()) + (v3.getY() - v2.getY()) ));
    }

    public String toString(){
        return "MyRectangle[v2=" + v2.toString() + ",v3=" + v3.toString()+"]";
    }
}
