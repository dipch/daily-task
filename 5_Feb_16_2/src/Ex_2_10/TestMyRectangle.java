package Ex_2_10;

import Ex_2_8.MyPoint;


public class TestMyRectangle {
    public static void main(String[] args) {
        //MyPoint p1 = new MyPoint(0,0);
        MyPoint p2 = new MyPoint(8,1);
        MyPoint p3 = new MyPoint(2,5);

        MyRectangle r1 = new MyRectangle(p2,p3);
        System.out.println(r1.getArea());
        System.out.println(r1.getPerimeter());
        System.out.println(r1.toString());


        MyRectangle r2 = new MyRectangle(8,5,2,1);
        //System.out.println(t2.getPerimeter());
        System.out.println(r2.getArea());
        System.out.println(r2.getPerimeter());
        System.out.println(r2.toString());

    }
}
