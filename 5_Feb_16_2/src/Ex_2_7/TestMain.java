package Ex_2_7;



public class TestMain {
    public static void main(String[] args) {
        MyPoint m1 = new MyPoint(8,2);
        MyPoint m2 = new MyPoint(1,2);

        MyLine l1 = new MyLine(m1,m2);
        MyLine l2 = new MyLine(8,2,1,2);

        System.out.println("Line l1: "+ l1);
        System.out.println("Line l2: "+ l2);

        System.out.println("Length l2: "+ l2.getLength());
        System.out.println("Gradient l1: "+ l1.getGradient());

        l1.setBeginX(7);
        l1.setBeginY(1);

        System.out.println("Begin X l1: "+ l1.getBeginX());
        System.out.println("Begin Y l1: "+ l1.getBeginY());

        l1.setEndX(2);
        l1.setEndY(3);

        System.out.println("End X l1: "+ l1.getEndX());
        System.out.println("End Y l1: "+ l1.getEndY());

        l1.setBeginXY(8,2);
        l1.setEndXY(1,2);
        System.out.println("Begin XY l1: "+ l1.toString());
        System.out.println("End XY l1: "+ l1.toString());




    }
}
