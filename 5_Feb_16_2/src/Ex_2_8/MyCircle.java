package Ex_2_8;

public class MyCircle {
    private MyPoint center = new MyPoint(0,0);
    private int radius = 1;

    public MyCircle() {
        center = new MyPoint(0,0);
        this.radius = radius;
    }
    public MyCircle(int x, int y, int radius) {
        //center.setX(x);
        //center.setY(y);
        center = new MyPoint(x,y);
        this.radius = radius;
    }

    public MyCircle(MyPoint center, int radius) {
        this.center = center;
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }
    public void setRadius(int r) {
        radius = r;
    }

    public MyPoint getCenter() {
        return center;
    }

    public void setCenter(MyPoint p) {
        center = p;
    }

    public int getCenterX(){
        return center.getX();
    }

    public void setCenterX(int x){
        center.setX(x);
    }
    public int getCenterY(){
        return center.getY();
    }
    public void setCenterY(int y){
        center.setY(y);
    }
    public int[] getCenterXY(){
        return new int[]{center.getX(), center.getY()};
    }
    public void setCenterXY(int x, int y){
        center.setX(x);
        center.setY(y);
    }
    public double getArea(){
        return Math.PI* radius * radius;
    }
    public double getCircumference() {
        return 2* Math.PI* radius;
    }
    public double distance(MyCircle another){
        return center.distance(another.center);
    }
    public String toString() {
        return "MyCircle[radius="+radius+",center="+center.toString()+"]";
    }













}
