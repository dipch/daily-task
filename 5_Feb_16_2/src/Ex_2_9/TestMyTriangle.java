package Ex_2_9;

import Ex_2_8.MyPoint;

public class TestMyTriangle {
    public static void main(String[] args) {
        MyPoint p1 = new MyPoint(0,0);
        MyPoint p2 = new MyPoint(0,3);
        MyPoint p3 = new MyPoint(3,0);

        MyTriangle t1 = new MyTriangle(p1,p2,p3);
        System.out.println(t1.getPerimeter());
        System.out.println(t1.getType());

        System.out.println(t1.toString());


        MyTriangle t2 = new MyTriangle(1,1,1,9,1,4);
        System.out.println(t2.getPerimeter());
        System.out.println(t2.getType());
        System.out.println(t2.toString());

    }
}