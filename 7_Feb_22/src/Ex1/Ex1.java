package Ex1;

public class Ex1 {
    public static void main(String[] args) {
        int i = 1;
        int j = 0;
        try{
            System.out.println("Try block entered" + " i = " + i + ", j = " + j);
            System.out.println("i/j = " + i/j);
            System.out.println("Try block exited");
        } catch(ArithmeticException e){
            System.out.println("Catch block entered");
            System.out.println("Arithmetic exception caught");
            System.out.println("Exception: " + e);
        }
        System.out.println("After try catch block");
    }

}
