package Ex1;

public class Ex2 {
    public static void main(String[] args) {
        int i = 12;
        for(int j = 3; j>= -1; j--) {
            try {
                System.out.println("Try block entered i = "+ i + ", j = " + j);
                System.out.println("i/j = " + i/j);
                System.out.println("Try block exited");
            }catch(ArithmeticException e) {
                System.out.println("Caught ArithmeticException"+ e);
            }finally {
                System.out.println("Finally block entered");
            }
        }
        System.out.println("After try-catch block");
    }
}
