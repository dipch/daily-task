package Ex1;

public class Ex3 {
    public static void main(String[] args) {
        int i = 12;
        try {
            System.out.println("Try block entered");
            for(int j = 3; j >= -1; --j) {
                System.out.println("Try block entered");
                System.out.println(i / j);
            }
            System.out.println("Try block exited");
        }catch (ArithmeticException e) {
            System.out.println("Caught ArithmeticException "+ e);
        }catch (Exception e) {
            System.out.println("Caught Exception "+ e);
        }
    }
}
