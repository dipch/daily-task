package Ex1;

public class MultipleException {
    public static void main(String[] args) {
        try {
            throw new ArithmeticException("Division by zero");
            //throw new NullPointerException("Null pointer");
        } catch (ArithmeticException | NullPointerException e) {
            System.out.println("Exception" + e);
        }
    }
}

