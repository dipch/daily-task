package Ex1;

public class MultipleException1 {
        public static void main(String[] args) {
        try {
            throw new ArithmeticException("Exception 1");
        } catch (NullPointerException | ArithmeticException e) {
            try {
                throw new NullPointerException("Exception 2");
            } catch (ArithmeticException | NullPointerException e1) {
                try {
                    throw new Exception("Exception 3");
                } catch (Exception e2) {
                    System.out.println("Exception 1"+e);
                    System.out.println("Exception 2"+e1);
                    System.out.println("Exception 3"+e2);
                }finally{
                    System.out.println("Finally 3");
                }
            }finally{
                System.out.println("Finally 2");
            }
        }finally {
            System.out.println("Finally 1");
        }
    }
}

