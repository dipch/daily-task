package ArrayListExercises;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/*
Write a Java program to create a new array list,
add some colors (string) and print out the collection

 */
public class A1 {
    public static void Q1(){
        /*
Write a Java program to create a new array list,
add some colors (string) and print out the collection
 */
        ArrayList<String> a = new ArrayList<>();
        a.add("red");
        a.add("green");
        a.add("blue");

        for (String str : a){
            System.out.println(str);
        }
    }


    public static void Q2(){
        //Write a Java program to iterate through all elements in a array list.
        ArrayList<String> a = new ArrayList<>();
        a.add("red");
        a.add("green");
        a.add("blue");

        for (String str : a){
            System.out.println(str);
        }
    }

    public static void Q3(){
        //Write a Java program to insert an element into the array list at the first position
        ArrayList<String> a = new ArrayList<>();
        a.add(0, "red");
        a.add(1, "blue");

        for (String str : a){
            System.out.println(str);
        }
    }

    public static void Q4(){
        //Write a Java program to retrieve an element (at a specified index) from a given array list
        ArrayList<String> a = new ArrayList<>();
        a.add(0, "red");
        a.add(1, "blue");

        System.out.println(a.get(0));
        System.out.println(a.get(1));
    }

    public static void Q5(){
        // Write a Java program to update specific array element by given element
        ArrayList<String> a = new ArrayList<>();
        a.add(0, "red");
        a.add(1, "blue");

        a.set(0, "green");

        for (String str : a){
            System.out.println(str);
        }
    }

    public static void Q6(){
        // Write a Java program to remove third element from an array list at a given index
        ArrayList<String> a = new ArrayList<>();
        a.add(0, "red");
        a.add(1, "blue");

        a.remove(0);

        for (String str : a){
            System.out.println(str);
        }
    }

    public static void Q7(){
        //Write a Java program to search an element in a array list.
        ArrayList<String> a = new ArrayList<>();
        a.add(0, "red");
        a.add(1, "blue");
        a.add(2, "green");

        System.out.println(a.indexOf("blue"));

    }

    public static void Q8(){
        //Write a Java program to sort a given array list.
        ArrayList<String> a = new ArrayList<>();
        a.add(0, "red");
        a.add(1, "blue");
        a.add(2, "green");

        Collections.sort(a);
        for (String str : a){
            System.out.println(str);
        }
    }
    @SuppressWarnings("all")
    public static void Q9(){
        //Write a Java program to copy one array list into another
        ArrayList<String> a = new ArrayList<String>();
        ArrayList<String> b = new ArrayList<String>();
        a.add(0, "red");
        a.add(1, "blue");
        a.add(2, "green");
        //Collections.copy(b,a);
        b.addAll(a);
        for (String str : b){
            System.out.println(str);
        }

    }
    @SuppressWarnings("all")
    public static void Q10(){
        //Write a Java program to shuffle elements in a array list.
        ArrayList<String> a = new ArrayList<>();
        a.add(0, "red");
        a.add(1, "blue");
        a.add(2, "green");
        Collections.shuffle(a);
        for (String str : a){
            System.out.println(str);
        }
    }
    public static void Q11(){
        //Write a Java program to reverse elements in a array list
        ArrayList<String> a = new ArrayList<>();
        a.add(0, "red");
        a.add(1, "blue");
        a.add(2, "green");
        Collections.reverse(a);
//        System.out.println(a);
        for(String str : a){
            System.out.println(str);
        }
    }

    public static void Q12(){
        // Write a Java program to extract a portion of a array list.
        ArrayList<String> a = new ArrayList<String>();
        List<String> b = new ArrayList<>();
        a.add(0, "red");
        a.add(1, "blue");
        a.add(2, "green");

        b = a.subList(0,1);
        for (String str : b){
            System.out.println(b);
        }
    }

    @SuppressWarnings("all")
    public static void Q13(){
        //Write a Java program to compare two array lists
        ArrayList<String> a = new ArrayList<>();
        ArrayList<String> b = new ArrayList<>();

        a.add(0, "red");
        a.add(1, "blue");
        a.add(2, "green");

        b.add(0, "red");
        b.add(1, "blue");
        b.add(2, "green");

        System.out.println(a.equals(b));


        b.remove("blue");


        ArrayList<String> c = new ArrayList<>();
        for(String str: a){
            c.add(b.contains(str) ? "Yes" : "No");
        }
        System.out.println(c);
    }
    public static void main(String[] args) {
        Q1();
    }
}
