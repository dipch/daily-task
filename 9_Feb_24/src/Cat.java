import java.io.IOException;
import java.io.RandomAccessFile;

public class Cat {
    public static void cat(String named) throws Exception {
        RandomAccessFile input = null;
        String line = null;

        try {
            input = new RandomAccessFile(named, "r");
            while ((line = input.readLine()) != null) {
                System.out.println(line);
            }
            return;
        }catch (Exception e) {
            System.out.println("Error: " + e);
        } finally {
            input.close();
        }
    }
}