package HashMapExercise;

import java.util.Collections;
import java.util.HashMap;
import java.util.Set;

public class A2 {

    public static void Q1(){
        //Write a Java program to associate the specified value with the specified key in a HashMap
        HashMap<String, Integer> map = new HashMap<>();
        map.put("red", 10);
        map.put("green", 30);
        map.put("blue", 20);

        System.out.println(map);
    }

    public static void Q2(){
        //Write a Java program to count the number of key-value (size) mappings in a map.
        HashMap<String, Integer> map = new HashMap<>();
        map.put("red", 10);
        map.put("green", 30);
        map.put("blue", 20);
        System.out.println(map.size());
    }

    public static void Q3(){
        //Write a Java program to copy all of the mappings from the specified map to another map
        HashMap<String, Integer> map = new HashMap<>();
        HashMap<String, Integer> map2 = new HashMap<>();
        map.put("red", 10);
        map.put("green", 30);
        map.put("blue", 20);
        //Collections.copy(map, map2);
        map2 = (HashMap)map.clone();
        System.out.println(map2);
    }

    public static void Q4(){
        //Write a Java program to remove all of the mappings from a map.
        HashMap<String, Integer> map = new HashMap<>();
        map.put("red", 10);
        map.put("green", 30);
        map.put("blue", 20);
        map.clear();
        System.out.println(map);
    }

    public static void Q5(){
        //Write a Java program to check whether a map contains key-value mappings (empty) or no
        HashMap<String, Integer> map = new HashMap<>();
        map.put("red", 10);
        map.put("green", 30);
        map.put("blue", 20);

        System.out.println(map.isEmpty());
    }

    public static void Q6(){
        //Write a Java program to get a shallow copy of a HashMap instance.
        HashMap<String, Integer> map = new HashMap<>();
        map.put("red", 10);
        map.put("green", 30);
        map.put("blue", 20);

        HashMap<String, Integer> map2  = map;
        System.out.println(map2);

    }

    public static void Q7(){
        //Write a Java program to test if a map contains a mapping for the specified key
        HashMap<Integer, String> map = new HashMap<>();
        map.put(10, "red");
        map.put(20, "green");
        map.put(30, "blue");

        System.out.println(map.containsKey(1));
    }

    public static void Q8(){
        //Write a Java program to test if a map contains a mapping for the specified value
        HashMap<Integer, String> map = new HashMap<>();
        map.put(10, "red");
        map.put(20, "green");
        map.put(30, "blue");

        System.out.println(map.containsValue("green"));
    }

    public static void Q9(){
        //Write a Java program to create a set view of the mappings contained in a map.
        HashMap<Integer, String> map = new HashMap<>();
        map.put(10, "red");
        map.put(20, "green");
        map.put(30, "blue");

        Set keySet = map.keySet();


    }
    public static void Q10(){
        //Write a Java program to get the value of a specified key in a map.
        HashMap<Integer, String> map = new HashMap<>();
        map.put(10, "red");
        map.put(20, "green");
        map.put(30, "blue");

        System.out.println(map.get(10));
    }

    public static void Q11(){
        //Write a Java program to get a set view of the keys contained in this map
        HashMap<Integer, String> map = new HashMap<>();
        map.put(10, "red");
        map.put(20, "green");
        map.put(30, "blue");
        //Set keySet = new
        Set keySet = map.keySet();
        System.out.println(keySet);
        keySet.remove(10);
        System.out.println(keySet);
        System.out.println(map);

    }

    public static void Q12(){
        //Write a Java program to get a collection view of the values contained in this map.
        HashMap<Integer, String> map = new HashMap<>();
        map.put(10, "red");
        map.put(20, "green");
        map.put(30, "blue");
        System.out.println(map.values());
    }

    public static void main(String[] args) {
        //Q12();
    }

}
