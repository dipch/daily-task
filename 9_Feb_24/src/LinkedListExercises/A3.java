package LinkedListExercises;

import java.util.*;

public class A3 {
    public static void Q1(){
        //Write a Java program to append the specified element to the end of a linked list
        LinkedList<String> ll = new LinkedList<>();
        ll.add("A");
        ll.add("B");
        ll.addLast("C");
        ll.addFirst("D");
        ll.add(2, "E");
        System.out.println(ll);
    }

    public static void Q2(){
        //Write a Java program to iterate through all elements in a linked list.
        LinkedList<String> ll = new LinkedList<>();
        ll.add("A");
        ll.add("B");
        ll.addLast("C");
        ll.addFirst("D");
        ll.add(2, "E");
        for(String str: ll){
            System.out.println(str);
        }
    }
    public static void Q3(){
        //Write a Java program to iterate through all elements in a linked list starting at the specified positio
        LinkedList<String> ll = new LinkedList<>();
        ll.add("A");
        ll.add("B");
        ll.add("C");
        ll.add("D");
        ll.add("E");
        ListIterator<String> it = ll.listIterator(2);

        while(it.hasNext()){
            System.out.println(it.next());
        }

    }
    public static void Q4(){
        LinkedList<String> ll = new LinkedList<>();
        ll.add("A");
        ll.add("B");
        ll.add("C");
        ll.add("D");
        ll.add("E");

        ListIterator<String> it = ll.listIterator(ll.size());
        while(it.hasPrevious()){
            System.out.println(it.previous());
        }

    }

    public static void Q5(){
        //to insert the specified element at the specified position in the linked list.
        LinkedList<String> ll = new LinkedList<>();
        ll.add("A");
        ll.add("B");
        ll.addLast("C");
        ll.addFirst("D");
        ll.add(2, "E");
        System.out.println(ll);
    }

    public static void Q6(){
        //to insert elements into the linked list at the first and last position
        LinkedList<String> ll = new LinkedList<>();
        ll.add("A");
        ll.add("B");
        ll.addLast("C");
        ll.addFirst("D");
        ll.add(2, "E");
        System.out.println(ll);
    }

    public static void Q7(){
        //to insert the specified element at the front of a linked list
        LinkedList<String> ll = new LinkedList<>();
        ll.add("A");
        ll.add("B");
        ll.addLast("C");
        ll.addFirst("D");
        ll.add(2, "E");
        System.out.println(ll);
    }

    public static void Q8(){
        // to insert the specified element at the end of a linked list
        LinkedList<String> ll = new LinkedList<>();
        ll.add("A");
        ll.add("B");
        ll.addLast("C");
        ll.addFirst("D");
        ll.add(2, "E");
        System.out.println(ll);
    }

    public static void Q9(){
        //to insert some elements at the specified position into a linked list.
        LinkedList<String> ll = new LinkedList<>();
        ll.add("A");
        ll.add("B");
        ll.addLast("C");
        ll.addFirst("D");
        ll.add(2, "E");
        System.out.println(ll);
    }

    public static void Q10(){
        //to get the first and last occurrence of the specified elements in a linked list.
        LinkedList<String> ll = new LinkedList<>();
        ll.add("A");
        ll.add("B");
        ll.add("C");
        ll.add("D");
        ll.add("E");
        ll.add("D");
        System.out.println(ll.lastIndexOf("D"));
        System.out.println(ll.indexOf("D"));

    }

    public static void Q11(){
        // to display the elements and their positions in a linked list.
        LinkedList<String> ll = new LinkedList<>();
        ll.add("A");
        ll.add("B");
        ll.add("C");
        ll.add("D");
        ll.add("E");
        ll.add("D");
        for(String str: ll){
            System.out.println(ll.indexOf(str)+" "+str);
        }
    }

    public static void Q12(){
        // to remove a specified element from a linked list.
        LinkedList<String> ll = new LinkedList<>();
        ll.add("A");
        ll.add("B");
        ll.add("C");
        ll.add("D");
        ll.add("E");
        ll.add("D");
        ll.remove("E");
        System.out.println(ll);
        ll.remove(0);
        System.out.println(ll);

    }

    public static void Q13(){
        //to remove first and last element from a linked list.
        LinkedList<String> ll = new LinkedList<>();
        ll.add("A");
        ll.add("B");
        ll.add("C");
        ll.add("D");
        ll.add("E");
        ll.removeFirst();
        ll.removeLast();
        System.out.println(ll);
    }

    public static void Q14(){
        //to remove all the elements from a linked list.
        LinkedList<String> ll = new LinkedList<>();
        ll.add("A");
        ll.add("B");
        ll.add("C");
        ll.add("D");
        ll.add("E");
        ll.clear();
        System.out.println(ll);
    }

    public static void Q15(){
        //to swap two elements in a linked list.
        LinkedList<String> ll = new LinkedList<>();
        ll.add("A");
        ll.add("B");
        ll.add("C");
        ll.add("D");
        ll.add("E");
        Collections.swap(ll, 0,1);
        System.out.println(ll);
    }

    public static void Q16(){
        //to shuffle the elements in a linked list
        LinkedList<String> ll = new LinkedList<>();
        ll.add("A");
        ll.add("B");
        ll.add("C");
        ll.add("D");
        ll.add("E");
        Collections.shuffle(ll);
        System.out.println(ll);
    }

    public static void Q17(){
        //to join two linked lists
        LinkedList<String> ll = new LinkedList<>();
        ll.add("A");
        ll.add("B");
        ll.add("C");
        ll.add("D");
        ll.add("E");
        LinkedList<String> ll2 = new LinkedList<>();
        ll2.add("F");
        ll2.add("B");
        ll2.add("G");

        ll.addAll(ll2);
        System.out.println(ll);


    }
    @SuppressWarnings({"unchecked", "deprecated", "rawtypes"})
    public static void Q18(){
        //to clone an linked list to another linked list.
        LinkedList<String> ll = new LinkedList<>();
        ll.add("A");
        ll.add("B");
        ll.add("C");
        ll.add("D");
        ll.add("E");
        LinkedList<String> ll2 = (LinkedList) ll.clone();
        //Collections.copy(ll2,ll);
        System.out.println(ll2);

    }

    public static void Q19(){
        //to remove and return the first element of a linked list
        LinkedList<String> ll = new LinkedList<>();
        ll.add("A");
        ll.add("B");
        ll.add("C");
        ll.add("D");
        ll.add("E");
        ll.removeFirst();
        ll.removeLast();
        System.out.println(ll);
    }

    public static void Q20(){
        //to retrieve but does not remove, the first element of a linked list
        LinkedList<String> ll = new LinkedList<>();
        ll.add("A");
        ll.add("B");
        ll.add("C");
        ll.add("D");
        ll.add("E");

        System.out.println(ll.getFirst());
    }

    public static void Q21(){
        //to retrieve but does not remove, the last element of a linked list
        LinkedList<String> ll = new LinkedList<>();
        ll.add("A");
        ll.add("B");
        ll.add("C");
        ll.add("D");
        ll.add("E");

        System.out.println(ll.getLast());
        System.out.println(ll);
    }

    public static void Q22(){
        //to check if a particular element exists in a linked list.
        LinkedList<String> ll = new LinkedList<>();
        ll.add("A");
        ll.add("B");
        ll.add("C");
        ll.add("D");
        ll.add("E");

        System.out.println(ll.contains("C"));
    }

    public static void Q23(){
        //to convert a linked list to array list
        LinkedList<String> ll = new LinkedList<>();
        ll.add("A");
        ll.add("B");
        ll.add("C");
        ll.add("D");
        ll.add("E");

        List<String> list = new ArrayList<>(ll);
        System.out.println(list);

    }

    public static void Q24(){
        //to compare two linked lists
        LinkedList<String> ll = new LinkedList<>();
        ll.add("A");
        ll.add("B");
        ll.add("C");
        ll.add("D");
        ll.add("E");

        LinkedList<String> l = new LinkedList<>();
        l.add("A");
        l.add("B");
        l.add("C");
        l.add("D");
        l.add("E");

        System.out.println(l.equals(ll));
        l.remove("B");
        l.remove("E");

        LinkedList<String> c = new LinkedList<>();
        for(String str: ll){
            c.add(l.contains(str) ? "Yes" : "No");
        }
        System.out.println(c);
    }

    @SuppressWarnings("all")
    public static void Q25(){
        //to test an linked list is empty or not
        LinkedList<String> ll = new LinkedList<>();
        ll.add("A");
        ll.add("B");
        System.out.println(ll.isEmpty());
        ll.remove("A");
        ll.remove("B");
        System.out.println(ll.isEmpty());
    }

    public static void Q26(){
        //to replace an element in a linked list
        LinkedList<String> ll = new LinkedList<>();
        ll.add("A");
        ll.add("B");
        ll.add("C");
        ll.add("D");
        ll.add("E");
        ll.set(1, "F");
        System.out.println(ll);
    }









    public static void main(String[] args) {
        Q26();
    }
}
