import java.io.*;
import java.util.Vector;

public class ListOfNumbers {
    private static Vector victor;
    private static final int size = 10;

    public static void readList() throws Exception {
        BufferedReader in = null;
        String file = "/home/dipc/celloscope/daily-task/9_Feb_24/src/num.txt";
        victor = new Vector(size);
        try{
            in = new BufferedReader(new FileReader(file));
            for (int i = 0; i < size; i++) {
                victor.addElement(in.readLine());
                System.out.println(victor.elementAt(i));
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            in.close();
        }
    }

    public ListOfNumbers () {
        victor = new Vector(size);
        for (int i = 0; i < size; i++)
            victor.addElement(new Integer(i));
    }
    public void writeList() {
        PrintStream out = null;

        try {
            System.out.println("Entering try statement");
            out = new PrintStream(new FileOutputStream("OutFile.txt"));

            for (int i = 0; i < size; i++)
                out.println("Value at: " + i + " = " + victor.elementAt(i));
        } catch (ArrayIndexOutOfBoundsException e) {
            System.err.println("Caught ArrayIndexOutOfBoundsException: " +
                    e.getMessage());
        } catch (IOException e) {
            System.err.println("Caught IOException: " + e.getMessage());
        } finally {
            if (out != null) {
                System.out.println("Closing PrintStream");
                out.close();
            } else {
                System.out.println("PrintStream not open");
            }
        }
    }

    public static void main(String[] args){
        try{
            readList();
        }catch(Exception e){
            System.out.println(e);
        }

    }
}